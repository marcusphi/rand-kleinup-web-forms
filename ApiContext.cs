using Microsoft.EntityFrameworkCore;
using rand_kleinup_web_forms.DbModels;

namespace rand_kleinup_web_forms
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=data/WebForms.db;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>().ToTable("Clients");
            modelBuilder.Entity<Employee>().ToTable("Employees");
            modelBuilder.Entity<DataForm>().ToTable("DataForms");
            modelBuilder.Entity<DataField>().ToTable("DataFields");
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<DataForm> DataForms { get; set; }
        public DbSet<DataField> DataFields { get; set; }
    }
}