using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rand_kleinup_web_forms.DbModels;
using rand_kleinup_web_forms.Dtos;

namespace rand_kleinup_web_forms.ApiControllers
{
    public class ClientsController : Controller
    {
        private readonly ApiContext _context;

        public ClientsController(ApiContext context)
        {
            _context = context;
        }

        [Authorize(Policy = "RequiresAdmin")]
        public async Task<IActionResult> Get(int? id)
        {
            var clients = await _context.Clients
                .Where(c => !id.HasValue || c.Id == id.Value)
                .Include(c => c.AssignedEmployee)
                .Include(c => c.FormsInUse)
                .Select(c => ClientDto.FromDbModel(c))
                .ToArrayAsync();

            return Ok(clients);
        }

        [Authorize(Policy = "RequiresAdmin")]
        public async Task<IActionResult> Create([FromBody] ClientDto dto)
        {
            var lastId = await _context.Clients.Select(c => c.Id).Concat(new[] { 0 }).MaxAsync();
            var client = new Client() { Id = lastId + 1 };
            dto.UpdateDbModel(client, _context);
            await _context.Clients.AddAsync(client);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [Authorize(Policy = "RequiresAdmin")]
        public async Task<IActionResult> Update([FromBody] ClientDto dto)
        {
            var client = await _context.Clients.Include(c => c.FormsInUse).FirstOrDefaultAsync(c => c.Id == dto.Id);
            dto.UpdateDbModel(client, _context);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [Authorize(Policy = "RequiresAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            var client = await _context.Clients.FirstOrDefaultAsync(c => c.Id == id);
            _context.Clients.Remove(client);
            await _context.SaveChangesAsync();
            return Ok();
        }

        [Authorize(Policy = "RequiresClient")]
        public async Task<IActionResult> GetForms()
        {
            var clientId = int.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var client = await _context.Clients.Include(c => c.FormsInUse).ThenInclude(f => f.Fields).FirstOrDefaultAsync(c => c.Id == clientId);
            var forms = client.FormsInUse.Select(f => DataFormDto.FromDbModel(f));
            return Ok(forms);
        }

        [Authorize(Policy = "RequiresClient")]
        public async Task<IActionResult> GetAssignedEmployee()
        {
            var clientId = int.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var client = await _context.Clients.Include(c => c.AssignedEmployee).FirstOrDefaultAsync(c => c.Id == clientId);
            return Ok(EmployeeDto.FromDbModel(client.AssignedEmployee));
        }
    }
}