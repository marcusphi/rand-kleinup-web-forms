using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rand_kleinup_web_forms.DbModels;
using rand_kleinup_web_forms.Dtos;

namespace rand_kleinup_web_forms.ApiControllers
{
    [Authorize(Policy = "RequiresAdmin")]
    public class DataFieldsController : Controller
    {
        private readonly ApiContext _context;

        public DataFieldsController(ApiContext context)
        {
            _context = context;
        }


        public async Task<IActionResult> AddOrUpdateToForm(int id, [FromBody] DataFieldDto dto)
        {
            var form = await _context.DataForms.Include(f => f.Fields).FirstOrDefaultAsync(f => f.Id == id);
            if (dto.Id == 0)
            {
                var lastId = await _context.DataFields.Select(fd => fd.Id).Concat(new[] { 0 }).MaxAsync();
                var field = new DataField() { Id = lastId + 1 };
                dto.UpdateDbModel(field);
                form.Fields.Add(field);
            }
            else
            {
                var field = await _context.DataFields.FirstOrDefaultAsync(fd => fd.Id == dto.Id);
                dto.UpdateDbModel(field);
            }

            await _context.SaveChangesAsync();
            return Ok();
        }

        public async Task<IActionResult> Delete(int id)
        {
            var field = await _context.DataFields.FirstOrDefaultAsync(fd => fd.Id == id);
            _context.DataFields.Remove(field);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}