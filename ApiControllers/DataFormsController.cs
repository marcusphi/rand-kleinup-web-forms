using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rand_kleinup_web_forms.DbModels;
using rand_kleinup_web_forms.Dtos;

namespace rand_kleinup_web_forms.ApiControllers
{
    [Authorize(Policy = "RequiresAdmin")]
    public class DataFormsController : Controller
    {
        private readonly ApiContext _context;

        public DataFormsController(ApiContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Get(int? id)
        {
            var forms = await _context.DataForms
                .Where(f => !id.HasValue || f.Id == id.Value)
                .Include(f => f.Fields)
                .Select(f => DataFormDto.FromDbModel(f))
                .ToArrayAsync();

            return Ok(forms);
        }

        public async Task<IActionResult> Create([FromBody] DataFormDto dto)
        {
            var lastId = await _context.DataForms.Select(f => f.Id).Concat(new[] { 0 }).MaxAsync();
            var form = new DataForm() { Id = lastId + 1 };
            dto.UpdateDbModel(form, _context);
            await _context.DataForms.AddAsync(form);
            await _context.SaveChangesAsync();
            return Ok();
        }

        public async Task<IActionResult> Update([FromBody] DataFormDto dto)
        {
            var form = await _context.DataForms.FirstOrDefaultAsync(f => f.Id == dto.Id);
            dto.UpdateDbModel(form, _context);
            await _context.SaveChangesAsync();
            return Ok();
        }

        public async Task<IActionResult> Delete(int id)
        {
            var form = await _context.DataForms.FirstOrDefaultAsync(f => f.Id == id);
            _context.DataForms.Remove(form);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}