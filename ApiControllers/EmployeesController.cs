using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rand_kleinup_web_forms.DbModels;
using rand_kleinup_web_forms.Dtos;

namespace rand_kleinup_web_forms.ApiControllers
{
    [Authorize(Policy = "RequiresAdmin")]
    public class EmployeesController : Controller
    {
        private readonly ApiContext _context;

        public EmployeesController(ApiContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Get(int? id)
        {
            var employees = await _context.Employees
                .Where(e => !id.HasValue || e.Id == id.Value)
                .Select(e => EmployeeDto.FromDbModel(e))
                .ToArrayAsync();

            return Ok(employees);
        }

        public async Task<IActionResult> Create([FromBody] EmployeeDto dto)
        {
            var lastId = await _context.Employees.Select(e => e.Id).Concat(new[] { 0 }).MaxAsync();
            var employee = new Employee() { Id = lastId + 1 };
            dto.UpdateDbModel(employee, _context);
            await _context.Employees.AddAsync(employee);
            await _context.SaveChangesAsync();
            return Ok();
        }

        public async Task<IActionResult> Update([FromBody] EmployeeDto dto)
        {
            var employee = await _context.Employees.FirstOrDefaultAsync(e => e.Id == dto.Id);
            dto.UpdateDbModel(employee, _context);
            await _context.SaveChangesAsync();
            return Ok();
        }

        public async Task<IActionResult> Delete(int id)
        {
            var employee = await _context.Employees.FirstOrDefaultAsync(e => e.Id == id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
            return Ok();
        }
    }
}