using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using rand_kleinup_web_forms.DbModels;

namespace rand_kleinup_web_forms.ApiControllers
{
    public class ListsController : Controller
    {
        public IActionResult FieldTypes()
        {
            var types = Enum.GetValues(typeof(FieldType)).Cast<FieldType>().Select(e => new { Type = e, Name = GetDisplayName(e) });
            return Ok(types);
        }

        public IActionResult States()
        {
            return Ok(new[]{
                new { Name = "Alabama", Abbrev = "AL"},
                new { Name = "Alaska", Abbrev = "AK"},
                new { Name = "Arizona", Abbrev = "AZ"},
                new { Name = "Arkansas", Abbrev = "AR"},
                new { Name = "California", Abbrev = "CA"},
                new { Name = "Colorado", Abbrev = "CO"},
                new { Name = "Connecticut", Abbrev = "CT"},
                new { Name = "Delaware", Abbrev = "DE"},
                new { Name = "Florida", Abbrev = "FL"},
                new { Name = "Georgia", Abbrev = "GA"},
                new { Name = "Hawaii", Abbrev = "HI"},
                new { Name = "Idaho", Abbrev = "ID"},
                new { Name = "Illinois", Abbrev = "IL"},
                new { Name = "Indiana", Abbrev = "IN"},
                new { Name = "Iowa", Abbrev = "IA"},
                new { Name = "Kansas", Abbrev = "KS"},
                new { Name = "Kentucky", Abbrev = "KY"},
                new { Name = "Louisiana", Abbrev = "LA"},
                new { Name = "Maine", Abbrev = "ME"},
                new { Name = "Maryland", Abbrev = "MD"},
                new { Name = "Massachusetts", Abbrev = "MA"},
                new { Name = "Michigan", Abbrev = "MI"},
                new { Name = "Minnesota", Abbrev = "MN"},
                new { Name = "Mississippi", Abbrev = "MS"},
                new { Name = "Missouri", Abbrev = "MO"},
                new { Name = "Montana", Abbrev = "MT"},
                new { Name = "Nebraska", Abbrev = "NE"},
                new { Name = "Nevada", Abbrev = "NV"},
                new { Name = "New Hampshire", Abbrev = "NH"},
                new { Name = "New Jersey", Abbrev = "NJ"},
                new { Name = "New Mexico", Abbrev = "NM"},
                new { Name = "New York", Abbrev = "NY"},
                new { Name = "North Carolina", Abbrev = "NC"},
                new { Name = "North Dakota", Abbrev = "ND"},
                new { Name = "Ohio", Abbrev = "OH"},
                new { Name = "Oklahoma", Abbrev = "OK"},
                new { Name = "Oregon", Abbrev = "OR"},
                new { Name = "Pennsylvania", Abbrev = "PA"},
                new { Name = "Rhode Island", Abbrev = "RI"},
                new { Name = "South Carolina", Abbrev = "SC"},
                new { Name = "South Dakota", Abbrev = "SD"},
                new { Name = "Tennessee", Abbrev = "TN"},
                new { Name = "Texas", Abbrev = "TX"},
                new { Name = "Utah", Abbrev = "UT"},
                new { Name = "Vermont", Abbrev = "VT"},
                new { Name = "Virginia", Abbrev = "VA"},
                new { Name = "Washington", Abbrev = "WA"},
                new { Name = "West Virginia", Abbrev = "WV"},
                new { Name = "Wisconsin", Abbrev = "WI"},
                new { Name = "Wyoming", Abbrev = "WY"},
                new { Name = "Washington, DC", Abbrev = "DC"},
            });
        }

        private static string GetDisplayName<T>(T t)
        {
            var memberInfo = typeof(T).GetMember(t.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                var attributes = memberInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                if (attributes != null && attributes.Length > 0)
                    return ((DisplayAttribute)attributes[0]).Name;
            }
            return t.ToString();
        }
    }
}