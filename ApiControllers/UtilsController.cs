using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using rand_kleinup_web_forms.Settings;

namespace rand_kleinup_web_forms.ApiControllers
{
    public class UtilsController : Controller
    {
        public readonly ApiContext _context;
        public readonly IOptions<SmtpSettings> _smtpSettings;

        public UtilsController(ApiContext context, IOptions<SmtpSettings> smtpSettings)
        {
            _context = context;
            _smtpSettings = smtpSettings;
        }

        [Authorize(Policy = "RequiresClient")]
        public async Task<IActionResult> SendEmail([FromBody] EmailDto email)
        {
            var clientId = int.Parse(HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var client = await _context.Clients.Include(c => c.AssignedEmployee).FirstOrDefaultAsync(c => c.Id == clientId);

            if (!email.HasGreetings)
            {
                var greeting = "Greetings, " + client.AssignedEmployee.Name + ",\n\n";
                greeting += "These form answers are filled by " + client.BusinessName + "\n\n";
                var ending = "\nOn behalf of " + client.BusinessName;

                email.Body = greeting + email.Body + ending;
            }

            using (var smtp = new SmtpClient(_smtpSettings.Value.Host, _smtpSettings.Value.Port))
            {
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                smtp.Credentials = new NetworkCredential(_smtpSettings.Value.Username, _smtpSettings.Value.Password);

                var mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(_smtpSettings.Value.Username);
                mailMessage.To.Add(client.AssignedEmployee.Email);
                if (!string.IsNullOrEmpty(client.Email))
                    mailMessage.To.Add(client.Email);
                mailMessage.Body = email.Body;
                mailMessage.Subject = email.Subject;
                await smtp.SendMailAsync(mailMessage);
            }

            return Ok();
        }
    }

    public class EmailDto
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool HasGreetings { get; set; }
    }
}