﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.Extensions.Options;
using rand_kleinup_web_forms.Settings;

namespace rand_kleinup_web_forms.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApiContext _context;
        private readonly IOptions<AdminSettings> _adminSettings;

        public HomeController(ApiContext context, IOptions<AdminSettings> adminSettings)
        {
            _context = context;
            _adminSettings = adminSettings;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] UserCredentials userCredentials)
        {
            UserInfo userInfo = null;
            if (userCredentials.IsAdmin)
            {
                var adminUsername = _adminSettings.Value.Username;
                if (string.IsNullOrEmpty(adminUsername))
                    adminUsername = "admin";
                var adminPassword = _adminSettings.Value.Password;
                if (string.IsNullOrEmpty(adminPassword))
                    adminPassword = "admin";

                if (userCredentials.Username == adminUsername && userCredentials.Password == adminPassword)
                    userInfo = new UserInfo() { Id = -1, Name = "Admin", IsAdmin = true };
            }
            else
            {
                var client = await _context.Clients.FirstOrDefaultAsync(c => c.BusinessCode == userCredentials.Username && c.Password == userCredentials.Password);
                if (client != null)
                    userInfo = new UserInfo() { Id = client.Id, Name = client.BusinessCode, IsAdmin = false };
            }
            if (userInfo == null)
                return Ok(new { Success = false, Error = "Invalid credentials" });

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, GetClaimsPrincipal(userInfo));
            return Ok(new { Success = true, RedirectTo = userInfo.IsAdmin ? "/Home/Admin" : "/Home/Client" });
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login");
        }

        [Authorize(Policy = "RequiresAdmin")]
        public IActionResult Admin()
        {
            return View();
        }

        [Authorize(Policy = "RequiresClient")]
        public IActionResult Client()
        {
            return View();
        }

        private static ClaimsPrincipal GetClaimsPrincipal(UserInfo userInfo)
        {
            var identity = new ClaimsIdentity(new Claim[] {
                new Claim(ClaimTypes.NameIdentifier, userInfo.Id.ToString()),
                new Claim(ClaimTypes.Name, userInfo.Name),
                new Claim(ClaimTypes.Role, userInfo.IsAdmin ? "Admin" : "Client")
            });
            var principal = new ClaimsPrincipal(identity);
            return principal;
        }
    }

    public class UserCredentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }

    public class UserInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAdmin { get; set; }
    }
}
