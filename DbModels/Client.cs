using System.Collections.Generic;

namespace rand_kleinup_web_forms.DbModels
{
    public class Client
    {
        public int Id { get; set; }
        public string BusinessName { get; set; }
        public string BusinessCode { get; set; }
        public string Password { get; set; }
        public string ContactName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public Employee AssignedEmployee { get; set; }
        public List<DataForm> FormsInUse { get; set; }
    }
}