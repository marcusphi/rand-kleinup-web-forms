using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace rand_kleinup_web_forms.DbModels
{
    public class DataForm
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [InverseProperty("Form")]
        public List<DataField> Fields { get; set; }
    }

    public class DataField
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public FieldType Type { get; set; }
        public bool IsRequired { get; set; }
        public DataForm Form { get; set; }
    }

    public enum FieldType
    {
        Text,
        Number,
        Date,
        State,
        [Display(Name = "Tax ID")]
        TaxId,
        [Display(Name = "Long Text")]
        LongText
    }
}