using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace rand_kleinup_web_forms.DbModels
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        [InverseProperty("AssignedEmployee")]
        public List<Client> AssignedClients { get; set; }
    }
}