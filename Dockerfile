FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2

# Copy starting database
WORKDIR /app/data
COPY --from=build-env /app/data/WebForms.db .
VOLUME /app/data

# Copy settings
WORKDIR /app/settings
COPY --from=build-env /app/settings/settings.json .
VOLUME /app/settings

# Copy built binaries
WORKDIR /app
COPY --from=build-env /app/out .


ENTRYPOINT ["dotnet", "rand-kleinup-web-forms.dll"]
