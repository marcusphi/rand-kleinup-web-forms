using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using rand_kleinup_web_forms.DbModels;

namespace rand_kleinup_web_forms.Dtos
{
    public class ClientDto
    {
        public int Id { get; set; }
        public string BusinessName { get; set; }
        public string BusinessCode { get; set; }
        public string Password { get; set; }
        public string ContactName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public int AssignedEmployeeId { get; set; }
        public string AssignedEmployeeName { get; set; }
        public List<int> FormsInUseIds { get; set; }

        public static ClientDto FromDbModel(Client client)
        {
            return new ClientDto()
            {
                Id = client.Id,
                BusinessName = client.BusinessName,
                BusinessCode = client.BusinessCode,
                Password = client.Password,
                ContactName = client.ContactName,
                Phone1 = client.Phone1,
                Phone2 = client.Phone2,
                Email = client.Email,
                AssignedEmployeeId = client.AssignedEmployee != null ? client.AssignedEmployee.Id : 0,
                AssignedEmployeeName = client.AssignedEmployee != null ? client.AssignedEmployee.Name : null,
                FormsInUseIds = client.FormsInUse != null ? client.FormsInUse.Select(f => f.Id).ToList() : new List<int>()
            };
        }

        public async void UpdateDbModel(Client client, ApiContext context)
        {
            client.BusinessName = BusinessName;
            client.BusinessCode = BusinessCode;
            client.Password = Password;
            client.ContactName = ContactName;
            client.Phone1 = Phone1;
            client.Phone2 = Phone2;
            client.Email = Email;
            client.AssignedEmployee = await context.Employees.FirstOrDefaultAsync(x => x.Id == AssignedEmployeeId);
            client.FormsInUse = await context.DataForms.Where(f => FormsInUseIds != null && FormsInUseIds.Contains(f.Id)).ToListAsync();
        }
    }
}