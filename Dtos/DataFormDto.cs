using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using rand_kleinup_web_forms.DbModels;

namespace rand_kleinup_web_forms.Dtos
{
    public class DataFormDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<DataFieldDto> Fields { get; set; }

        public static DataFormDto FromDbModel(DataForm form)
        {
            return new DataFormDto()
            {
                Id = form.Id,
                Name = form.Name,
                Fields = form.Fields.Select(f => DataFieldDto.FromDbModel(f)).ToList(),
            };
        }

        public async void UpdateDbModel(DataForm form, ApiContext context)
        {
            form.Name = Name;
            form.Fields = Fields != null ? await context.DataFields.Where(fd => Fields.Any(f => f.Id == fd.Id)).ToListAsync() : new List<DataField>();
        }
    }

    public class DataFieldDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public FieldType Type { get; set; }
        public bool IsRequired { get; set; }

        public static DataFieldDto FromDbModel(DataField field)
        {
            return new DataFieldDto()
            {
                Id = field.Id,
                Name = field.Name,
                Type = field.Type,
                IsRequired = field.IsRequired,
            };
        }

        public void UpdateDbModel(DataField field)
        {
            field.Name = Name;
            field.Type = Type;
            field.IsRequired = IsRequired;
        }
    }
}