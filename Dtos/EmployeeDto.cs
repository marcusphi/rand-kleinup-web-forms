using System;
using rand_kleinup_web_forms.DbModels;

namespace rand_kleinup_web_forms.Dtos
{
    public class EmployeeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public static EmployeeDto FromDbModel(Employee employee)
        {
            return new EmployeeDto()
            {
                Id = employee.Id,
                Name = employee.Name,
                Email = employee.Email
            };
        }

        public void UpdateDbModel(Employee employee, ApiContext context)
        {
            employee.Name = Name;
            employee.Email = Email;
        }
    }
}