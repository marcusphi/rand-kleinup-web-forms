# rand-kleinup-web-forms

A small web app contracted by Rand Kleinup via Upwork, used for employee to set up forms, send to client to fill, and receive reponse as text data via email.

### Build

```sh
docker build -t <IMAGE_NAME> .
```

### Run

```sh
docker container run [-d --restart always -p PORT:80 --name CONTAINER_NAME -v $(pwd)/data:/app/data -v $(pwd)/settings:/app/settings] <IMAGE_NAME>
```
